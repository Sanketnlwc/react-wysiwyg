# React WYSIWYG Editor

React WYSIWYG Editor is html editor which in addition to the normal text formatting also lets you to :

- Create multiple posts 

- Edit/update the created text 

- Preview the post 

- Import/Export the created content.

## Demo

Live : [React WYSIWYG Editor](https://react-editor-d3a4a.firebaseapp.com).

## Getting Started


### Prerequisites

You need to have node installed in the local. If not install it from [Node Js](https://nodejs.org/en/) official website

### Downloading

Navigate [Here](https://bitbucket.org/Sanketnlwc/react-wysiwyg/downloads/)

Click on Download Repository

### Installing

```

npm install
npm start

```
### Screenshots

![alt text](img/DesktopSS.png "Desktop Version of editor")
![alt text](img/MobileSS.png "Mobile Version of editor")
