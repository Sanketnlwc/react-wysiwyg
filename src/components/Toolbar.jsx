import React, { Component } from 'react';
import FileSaver from 'file-saver';
import Dropzone from 'react-dropzone'
import $ from 'jquery';
import {  Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class Toolbar extends Component {
    constructor(){
        super();
    this.state = {
      imageModal: false,
      deleteImagePopover: false,
      isEdit:false
    };
    this.toggleImageModal = this.toggleImageModal.bind(this);
    this.handleFileSelect = this.handleFileSelect.bind(this);
    }
    handleFileSelect(evt) 
    {
        var files = evt.target.files; // FileList object

        // Loop through the FileList
        for (var i = 0; i< files.length ; i++) 
        {
            var isTxt = files[i].name.split('.').pop();
            if(isTxt === "txt")
            {
            var reader = new FileReader();
            reader.onload = function (event) {
           var contents = event.target.result;
            document.getElementById('editor').innerHTML=contents;
            }
            reader.readAsText(files[i]);
          }
          else{
              alert("Please upload files with .txt extension");
          }
        }
    }

    // toggle functions
      toggleImageModal() {
    this.setState({
      imageModal: !this.state.imageModal
    });
  }

    toggleDeleteImagePopover(){
        this.setState({
            deleteImagePopover: !this.state.deleteImagePopover
        })
    }

    // toggle functions ends

    // Handling different editor functionalities
    command(style){
        if (style === 'createlink') {
                    var url =  prompt("Enter a link","http://")
                    document.execCommand(style, false, url);
                     }
                else
                document.execCommand(style,false,null);
         }
    
    onDrop(acceptedFiles,rejectedFiles) {
        if(rejectedFiles.length > 0)
        {
            alert("Only jpeg and png images allowed");
            return;
        }
        acceptedFiles.forEach(file => {
        const reader = new FileReader();
        reader.onload = () => {
            $('.imageDragDrop img').attr('src',file.preview);
            $('.imageDragDrop img').css("display","block");
            this.setState({
                imageModal : !this.state.imageModal
            })
            };

        reader.readAsBinaryString(file);
         });
    }

    fileSave() {
    var fileName =  prompt("Enter filename");
    var blob = new Blob([document.getElementById("editor").innerText], {type: "text/plain;charset=utf-8"});
    FileSaver.saveAs(blob, fileName);
     }   

  render() {
    
    return (
        <div className="toolbar">
                 <button  onClick={() => this.command("bold")}  title="Bold"><i className="fas fa-bold"></i></button>
                <button  onClick={() => this.command("italic")} title="Italic"><i className="fas fa-italic" ></i></button>
                <button onClick={() => this.command("underline")}  title="Underline"><i className="fas fa-underline"></i></button>
                <div className="separator"></div>
                <button onClick={() => this.toggleImageModal()} title="Image"><i className="fas fa-image" ></i></button>
                <button onClick={() => this.command("createlink")}  title="Link"><i className="fas fa-link"></i></button>
                <button onClick={() => this.command("unlink")}  title="Unlink"><i className="fas fa-unlink"></i></button>
                <div className="separator"></div>
                <button onClick={() => this.command("justifyLeft")}  title="align left"><i className="fas fa-align-left"></i></button>
                <button onClick={() => this.command("justifyRight")}  title="align right"><i className="fas fa-align-right"></i></button>
                <button onClick={() => this.command("justifyCenter")}  title="align center"><i className="fas fa-align-center"></i></button>
                <button onClick={() => this.command("justifyFull")}  title="align justify"><i className="fas fa-align-justify"></i></button>
                <div className="separator"></div>
                <button onClick={() => this.command("insertUnorderedList")}  title="Bullets"><i className="fas fa-list-ul"></i></button>
                <button onClick={() => this.command("insertOrderedList")}  title="Numbered Bullets"><i className="fas fa-list-ol"></i></button>
                <div className="separator"></div>
                <button className="uploader" title="Import file"><i className="fas fa-file-upload"><input type="file" id="fileUpload" className="fileUpload" name="file" onInput={ (e) => this.handleFileSelect(e)} /></i></button>
                 <button onClick={() => this.fileSave()}  title="Export file"><i className="fas fa-file-export"></i></button>

              {/*image modal*/}
     <Modal isOpen={this.state.imageModal} toggle={this.toggleImageModal} id="imageDragDrop">
          <ModalHeader toggle={this.toggleImageModal}>Upload Image</ModalHeader>
          <ModalBody>
             <Dropzone onDrop={this.onDrop.bind(this)} className="uploader" accept="image/jpeg, image/png" multiple={false}>
           <strong> Drop Image </strong> <br/>
                or click here
          </Dropzone>
        </ModalBody>
          <ModalFooter>
            <Button color="default" onClick={() => this.toggleImageModal()}>Close</Button>
          </ModalFooter>
        </Modal>

   
            
          </div>
    );
  }
}

export default Toolbar;
