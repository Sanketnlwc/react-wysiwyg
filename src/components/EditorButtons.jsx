

import React, { Component } from 'react';
import { Button,  Modal, ModalHeader, ModalBody, ModalFooter  } from 'reactstrap';
import $ from 'jquery';


class EditorButtons extends Component {
 
  constructor(props) {
  super(props);
    this.state = {
      previewModal: false
    };
       this.togglePreviewModal = this.togglePreviewModal.bind(this);
    this.postData = this.postData.bind(this);
    this.updatePost = this.updatePost.bind(this);
    this.newPost = this.newPost.bind(this);
  }

  // create update preview posts
     newPost(){      
         $('#editor').html("");
        this.props.setStates("NP");
     }
     
    postData(){

        var today = new Date();
        var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
        var hours = today.getHours() < "10" ? "0" + today.getHours() : today.getHours();
        var minutes = today.getMinutes() < "10" ? "0" + today.getMinutes() : today.getMinutes();
        var time = hours + ":" + minutes;
        var dateTime = date+' | '+time;
        let contentJson = {
            contentHtml : document.getElementById("editor").innerHTML,
            contentText : document.getElementById("editor").innerText,
            contentNo : Math.random(),
            dateTime : dateTime
        }
        // this.setState({content: this.state.content.concat(contentJson)})
        this.props.setStates("PP",contentJson);
     }

    updatePost(srNo){
        var today = new Date();
        var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
        var time = today.getHours() + ":" + today.getMinutes();
        var dateTime = date+' | '+time;
        let contentCopy = this.props.content;
        for (var i = 0; i < contentCopy.length; i++) {
            if (contentCopy[i].contentNo === srNo) {
             contentCopy[i].contentHtml = document.getElementById("editor").innerHTML;
             contentCopy[i].contentText = document.getElementById("editor").innerText;
             contentCopy[i].dateTime = dateTime;
                        break;
                            }
            }
        //     this.setState({
        //     content: contentCopy
        // })
        this.props.setStates("UP",contentCopy);
    }

    togglePreviewModal(){
    this.setState({
      previewModal: !this.state.previewModal
    });
 }
 preview(){

          $('#previewModal .modal-body').html(document.getElementById("editor").innerHTML);
     }
  render() {
      var postButton = null;
       this.props.isEdit ? postButton = <Button color="primary" className="ml-2" onClick = {() => this.updatePost(this.props.srNo)}> Update </Button> : postButton = <Button color="info" className="ml-2" onClick={() => this.postData()}> Post </Button>
    return (
       <div className="buttons pull-right">
            <Button color="default" onClick={() => this.newPost()}>Write new post</Button>
            <Button color="success" className="ml-2" onClick={() => this.togglePreviewModal()}>Preview</Button>
            {postButton}

          {/*preview modal*/}
     <Modal isOpen={this.state.previewModal} toggle={this.togglePreviewModal} id="previewModal" onOpened={() => this.preview()} size="lg">
          <ModalHeader toggle={this.togglePreviewModal}>Preview</ModalHeader>
          <ModalBody>
        </ModalBody>
          <ModalFooter>
            <Button color="default" onClick={() => this.togglePreviewModal()}>Close</Button>
          </ModalFooter>
        </Modal>
    </div>

        
      );
  }
}

export default EditorButtons;
