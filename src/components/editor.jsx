import React, { Component } from 'react';

import $ from 'jquery';
import ToolbarComponent from './Toolbar';
import { Button , Popover, PopoverBody} from 'reactstrap';
class Editor extends Component {

   constructor(){
       super();
    this.state = {
      deleteImagePopover: false
    };
       
    this.toggleDeleteImagePopover = this.toggleDeleteImagePopover.bind(this);
    this.deleteImage = this.deleteImage.bind(this);
   }

    allowDrop(ev) {
    ev.preventDefault();
        }

    drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
    }

    drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    if(data === "PopoverDeleteImage")
    ev.target.appendChild(document.getElementById(data));
        }

    toggleDeleteImagePopover(){
        this.setState({
            deleteImagePopover: !this.state.deleteImagePopover
        })
    }
    
     deleteImage(){
        $('#editor img').css("display","none");
        this.setState({
            deleteImagePopover:!this.state.deleteImagePopover
        })
    }

   

  render() {
       return (
        <div>
            
        <div id="htmlEditor">
              <h1>A WYSIWYG Editor.</h1>
            <ToolbarComponent/>
             <div id="wrapperEdit">
         <div id='editor' contentEditable onDrop={(e) => this.drop(e)} onDragOver={(e) => this.allowDrop(e)}>
            <div className="imageDragDrop">
               <img tabIndex="0" src="" draggable="true" onDragStart={(e) => this.drag(e)} alt="" id="PopoverDeleteImage" onClick={() => this.toggleDeleteImagePopover()}/>
            </div>
             <p> Write here </p>
         </div>
      </div>
 
   {/*popover delete image */}

        <Popover placement="right" isOpen={this.state.deleteImagePopover} target="PopoverDeleteImage" toggle={this.toggleDeleteImagePopover}>
          <PopoverBody><Button color="danger" onClick={() => this.deleteImage()}> Delete </Button></PopoverBody>
        </Popover>

    </div>
</div>

        
    );
  }
}

export default Editor;
