import React, { Component } from 'react';
import { Button,  Modal, ModalHeader, ModalBody, ModalFooter  } from 'reactstrap';
import $ from 'jquery';


class ReadPosts extends Component {
    constructor(props){
        super(props);
    this.state = {
      readMoreModal: false,
      modalContent:""
    };
    this.togglereadMoreModal = this.togglereadMoreModal.bind(this);
    this.editPost = this.editPost.bind(this);
  }
  
  togglereadMoreModal(){
      this.setState({
        readMoreModal:!this.state.readMoreModal
      })
  }

    readMore(contentHtml){
      this.setState({
        readMoreModal:true,
        modalContent : contentHtml
      })
    }
   

      createMarkup(){
         return {__html: this.state.modalContent};
      }

      editPost(contentHtml,srNo){
         $('#editor').html(contentHtml);
        this.props.setStates("EP",srNo);
    }
      

  render() {
var postContent = null;
      if(this.props.data)
      {
        this.props.data.reverse();
        postContent = getPosts(this.props.data,this);
      }
    return (
        <div>
          <div className="header-tabs">
            <span> Recent Posts </span>
            </div>
            <div className="recent-content">
            {postContent}
            </div>


            {/*preview modal*/}
     <Modal isOpen={this.state.readMoreModal} toggle={this.togglereadMoreModal} id="readMoreModal"  size="lg">
          <ModalHeader toggle={this.togglePreviewModal}>Post</ModalHeader>
          <ModalBody dangerouslySetInnerHTML={ this.createMarkup()}>
        </ModalBody>
          <ModalFooter>
            <Button color="default" onClick={() => this.togglereadMoreModal()}>Close</Button>
          </ModalFooter>
        </Modal>
            </div>


            
    );
  }
}

function getPosts(data,obj)
{
  var myArray = null;
    myArray =  data.map(function (element) {
            return (
            <div className="posts">
             <div className="posts-body">
               <p>
            {element.contentText}
            </p>
            </div>
            <div className="posts-buttons p-1">
              <span>
              {element.dateTime}
              </span>
              <Button color="success" className="float-right mr-1" size="sm" onClick = { () =>  obj.readMore(element.contentHtml)}> View </Button>
              <Button color="default" className="float-right mr-1" size="sm" onClick = { () => obj.editPost(element.contentHtml,element.contentNo)}> Edit </Button>
              </div>
    </div>
            )
        })
    return myArray;    
}

export default ReadPosts;
