import React, { Component } from 'react';
import EditorComponent from './editor';
import '../css/App.css';
import ReadPostsComponent from './readPosts';
import EditorButtons from './EditorButtons';

class App extends Component {

  constructor(props) {
  super(props);
    this.state = {
      content:[],
      isEdit:false
    };
    this.setStates = this.setStates.bind(this);  
  }

  setStates(name,content){
    switch(name){
      case "NP":
               this.setState({
                     isEdit : false
                    })
                break;
      case "PP": this.setState({
                      content: this.state.content.concat(content)
                     })
                 break;
      case "EP":
                 this.setState({
                    isEdit : true,
                      srNo : content
                 })
                 break;
      case "UP":
                 this.setState({
                     content: content
                 })
                 break;
      default : break;

    }
  }

  render() {
    
    return (
      <div> 
        <div className="col-lg-1 float-left hidden-xs">
                    </div>
     <div className="wrapper col-xs-12 float-left col-lg-8">
      <EditorComponent />
      <EditorButtons isEdit = { this.state.isEdit } setStates = {this.setStates} srNo = { this.state.srNo} content = { this.state.content }/>
      </div>
      <div className="col-xs-12 col-lg-3 float-right rightSideBar">
       <ReadPostsComponent data = { this.state.content } setStates = {this.setStates} />
                    {/*onClick={this.editPost}/>*/}
        </div>
      </div>
    );
  }
}

export default App;
